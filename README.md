## clear_base64_image

## CONTENTS OF THIS FILE
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Supported configurations
 
## INTRODUCTION

The Clear Base64 Image module will transform image 
encoded in base64 contained in a node with image file


## REQUIREMENTS

This module will not requires any dependencies.

## INSTALLATION
 
 * Install as you would normally install a custom Drupal module. Visit:
   [https://www.drupal.org/docs/extending-drupal/installing-modules]
   for further information.

## CONFIGURATION

To display base64 image, Go to Administration » Configuration » 
 Media » manage image base64.

To settings the module, Go to Administration » Configuration » 
 Media » manage image base64 » Settings.


## Supported configurations
 * The destination directory for the image
 * The extension for the image file
