<?php

namespace Drupal\clear_base64_image\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Component\Render\FormattableMarkup;

/**
 * Form to manage and replace image base64 to jpeg or png.
 */
class ClearBase64ImageList extends ConfigFormBase {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Constructs a new ClearBase64ImageController object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, Connection $database, FileSystemInterface $file_system) {
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->database = $database;
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('database'),
      $container->get('file_system'),
      $container->get('link_generator'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'clear_base64_image_list_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['clear_base64_image.list'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get base64 image from database.
    $datas = $this->getInfoImage();

    // Get settings from the form.
    $directory = $this->config('clear_base64_image.settings')->get('directory');

    // Verify if the directory is writable.
    $public_folder = $this->fileSystem->realpath("public://") . "/";
    $writable = is_writable($public_folder . $directory);

    $header = [
      '',
      $this->t('Image'),
      $this->t('Node'),
      $this->t('Nid'),
    ];

    $rows = [];
    $i = 1;
    foreach ($datas as $data) {
      $rows[$i] = [
        new FormattableMarkup('<a href="@src" > Display </a>', ['@src' => $data['description']]),
        $data['descriptionSubstr'],
        $data['title'],
        $data["id"],
      ];

      $i++;
    }

    $form['table'] = [
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $rows,
      '#empty' => $this->t('Empty'),
    ];

    $form['pager'] = [
      '#type' => 'pager',
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Convert'),
      '#attributes' => [
        'disabled' => !$writable || !$rows,
      ],
    ];

    // Display directory.
    $message = $this->t('Target directory : @directory', ['@directory' => $public_folder . $directory]);
    $messenger = $this->messenger();
    $messenger->addMessage($message, $messenger::TYPE_WARNING);

    // Display warning message if directory is not writable.
    if (!$writable) {
      $message = $this->t('The directory is not writable');
      $messenger = $this->messenger();
      $messenger->addMessage($message, $messenger::TYPE_WARNING);
    }

    return $form;
  }

  /**
   * Get nodes that contain base64 image.
   *
   * @return array
   *   Returns nid that contain base64 images.
   */
  public function getImgDatabase() {
    $nodeStorage = $this->entityTypeManager->getStorage('node');
    $results = $nodeStorage->getQuery()->condition('body', 'data:image/', 'CONTAINS');
    $results = $results->execute();

    return $results;
  }

  /**
   * Get informations from the base64 images.
   *
   * @return array
   *   Returns informations from the base64 images.
   */
  public function getInfoImage() {
    $results = $this->getImgDatabase();
    $datas = [];

    foreach ($results as $result) {
      $node_storage = $this->entityTypeManager->getStorage('node');
      $nid = $result;
      $node = $node_storage->load($nid);

      $description = $node->body->value;

      $xpath = new \DOMDocument();
      $xpath->loadHTML($description);
      $img = $xpath->getElementsByTagName('img');

      foreach ($img as $link) {
        $imgTitle = $link->getAttribute('title');
        $imgSrc = $link->getAttribute('src');
        $linkHref = explode(',', $imgSrc);
        $linkHref = end($linkHref);

        if (base64_encode(base64_decode($linkHref, TRUE)) === $linkHref) {
          if (empty($imgTitle)) {
            $imgTitle = $link->getAttribute('src');
          }

          $datas[] = [
            'id' => $node->nid->value,
            'title' => $node->toLink(),
            'descriptionSubstr' => substr($imgTitle, 0, 70),
            'description' => $link->getAttribute('src'),
            'imgTitle' => $link->getAttribute('title'),
          ];
        }
      }

    }

    return $datas;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $public_folder = $this->fileSystem->realpath("public://") . "/";
    $directory = $this->config('clear_base64_image.settings')->get('directory');
    $writable = is_writable($public_folder . $directory);

    if (!$writable) {
      $form_state->setErrorByName('directory', $this->t("The directory is not writable"));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $selected_names = [];
    $fields = array_filter($form_state->getValue('table'));
    $datas = $this->getInfoImage();

    foreach ($fields as $field) {
      $selected_names[] = $datas[$field - 1];
    }

    $batch = [
      'title' => $this->t('Processing'),
      'operations' => [],
      'finished' => '\Drupal\clear_base64_image\Form\ClearBase64ImageList::updateMessage',
    ];

    foreach ($selected_names as $selected) {
      $batch['operations'][] = [
        '\Drupal\clear_base64_image\Form\ClearBase64ImageList::replace',
        [$selected],
      ];
    }

    batch_set($batch);
  }

  /**
   * Status message.
   */
  public static function updateMessage($success) {
    if ($success) {
      $message = t('Success');
    }
    else {
      $message = t('Finished with an error.');
    }

    $messenger = \Drupal::messenger();
    $messenger->addMessage($message);
  }

  /**
   * Create image page controller.
   */
  public static function replace($selected) {
    // Get settings from the form.
    $directory = "public://" . \Drupal::config('clear_base64_image.settings')->get('directory');
    $imgType = \Drupal::config('clear_base64_image.settings')->get('type');

    $imgSrc = $selected["description"];

    $linkHref = explode(',', $imgSrc);
    $linkHref = end($linkHref);

    $nid = $selected["id"];
    $node_storage = \Drupal::EntityTypeManager()->getStorage('node');
    $node = $node_storage->load($nid);

    // If src is base64.
    if (base64_encode(base64_decode($linkHref, TRUE)) === $linkHref) {
      $decode = base64_decode($linkHref);
      $imgTitle = $selected["imgTitle"];

      $date = getdate();
      $date = $date['seconds'] . $date['minutes'] . $date['hours'] . $date['mday'] . $date['mon'] . $date['year'];

      if (empty($imgTitle)) {
        // If image don't have title.
        // Generate title with nid, parts of the base64 and the current date.
        $new_file = $directory . $node->nid->value . '-' . substr($linkHref, 120, 10) . '-' . $date . '.' . $imgType;
      }
      else {
        // If image already have title.
        // The file get the name of the image title.
        $new_file = $directory . $imgTitle . '.' . $imgType;
      }

      $file_exists = FileSystemInterface::EXISTS_REPLACE;
      file_save_data($decode, $new_file, $file_exists);
    }

    $body = $node->body->value;
    $xpath = new \DOMDocument();
    $xpath->loadHTML($body);

    $body = str_replace($imgSrc, file_url_transform_relative(file_create_url($new_file)), $body);

    $node->body->value = $body;
    $node->save();
  }

}
