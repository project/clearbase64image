<?php

namespace Drupal\clear_base64_image\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\File\FileSystemInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form to add a database entry, with all the interesting fields.
 */
class ClearBase64imageSettings extends ConfigFormBase {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Constructs a new ClearBase64ImageController object.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   */
  public function __construct(FileSystemInterface $file_system) {
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('file_system'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'clear_base64_image_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['clear_base64_image.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('clear_base64_image.settings');

    $form['directory'] = [
      '#type' => 'textfield',
      '#title' => $this
        ->t('Choose the destination directory'),
      '#default_value' => $config->get('directory'),
      '#required' => TRUE,
      '#description' => $this->t('Enter your public directory like "inline-images/"'),
    ];

    $form['type'] = [
      '#type' => 'radios',
      '#title' => $this
        ->t('Choose the extension'),
      '#default_value' => $config->get('type'),
      '#options' => [
        'png' => $this
          ->t('PNG'),
        'jpg' => $this
          ->t('JPG'),
      ],
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $public_folder = $this->fileSystem->realpath("public://") . "/";

    if (!file_exists($public_folder . $form_state->getValue('directory'))) {
      $form_state->setErrorByName('directory', $this->t("This path does not exist"));
    }

    if (substr($form_state->getValue('directory'), -1) != "/") {
      $form_state->setErrorByName('directory', $this->t("The path must finish with a <b> ' / ' </b>"));
    }

    if (!is_dir($public_folder . $form_state->getValue('directory'))) {
      $form_state->setErrorByName('directory', $this->t("This path is not a directory"));
    }

    if (!is_writable($public_folder . $form_state->getValue('directory'))) {
      $form_state->setErrorByName('directory', $this->t("The directory is not writable"));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->config('clear_base64_image.settings')
      ->set('directory', $form_state->getValue('directory'))
      ->set('type', $form_state->getValue('type'))
      ->save();

    parent::submitForm($form, $form_state);

  }

}
